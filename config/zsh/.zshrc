alias _check='type &>/dev/null'

TMPLOCAL="$HOME/.local/tmp"
mkdir -p "$TMPLOCAL"

# Mac Sucks
[[ -e '/usr/local/opt/coreutils/libexec/gnubin' ]] \
  && PATH='/usr/local/opt/coreutils/libexec/gnubin':"$PATH"

DOTFILES="${DOTFILES:-"$HOME/.dotfiles"}"
ZPLUG_HOME="${ZPLUG_HOME:-"$HOME/.zplug"}"

source "$DOTFILES/utils/zplug.zsh"
source "$DOTFILES/utils/spaceship.zsh"
zplug load

[ -s '/etc/profile.d/cnf.sh' ] && source '/etc/profile.d/cnf.sh'
[ -s '/usr/share/doc/find-the-command/ftc.zsh' ] && source '/usr/share/doc/find-the-command/ftc.zsh' noprompt noupdate quiet
[ -s "$HOME/.travis/travis.sh" ] && source "$HOME/.travis/travis.sh"
[ -s '/usr/local/bin/virtualenvwrapper.sh' ] && source '/usr/local/bin/virtualenvwrapper.sh'
[ -s "$HOME/.netlify/helper/path.zsh.inc" ] && source "$HOME/.netlify/helper/path.zsh.inc"
[ -s "$HOME/.config/tabtab/zsh/__tabtab.zsh" ] && source "$HOME/.config/tabtab/zsh/__tabtab.zsh"


# =============== #
# === ALIASES === #

# mkcd function
function mkcd () {
  mkdir -p -- "$1" \
  && cd -P -- "$1"
}

# ls with color and hyperlinks, please
alias ls='ls --hyperlink=auto --color=tty'

# always call docker with sudo
_check sudo && alias docker='sudo docker'
_check sudo && alias docker-compose='sudo docker-compose'

# alias for reset
alias reset='\reset; source ~/.zshrc'

# fix GREP_OPTIONS messages
alias grep='\grep $GREP_OPTIONS'
unset GREP_OPTIONS

unfunction ggf

# === VARIABLES ===

# GCC color setup
export GCC_COLORS='error=01;31:warning=01;33:note=01;36:caret=01;32:locus=01:quote=01'

# env variables for multi-core compilation
_makejobs=4
if _check nproc; then
  _nproc=$(nproc); (( _makejobs = _nproc > 8 ? 8 : _nproc ))
fi
export MAKEFLAGS="$MAKEFLAGS -j${_makejobs}"
export CARGO_BUILD_JOBS="${_makejobs}"

export PNPM_HOME="$HOME/.local/share/pnpm"

# Add local directories to the PATH
export PATH="$PNPM_HOME:$HOME/.dotnet/tools":"$HOME/.bin":"$HOME/.local/bin":"$PATH"

# For the love of everything, use nano!!
export VISUAL=nano
export EDITOR="$VISUAL"

export PAGER=less
export LESS=-irRSx4

export BAT_PAGER="less $LESS"

# Hack for Aseprite AUR package
export ASEPRITE_ACCEPT_EULA=yes

# ======================= #
# === PACKAGE MANAGER === #

# Pacaur/Yay package manager
_check yay && {
    alias pm='yay --sudoloop'
    alias pmnc='pm --noconfirm'

    alias pmupd='pmnc -Syu'

    alias pmin='pmnc -S'
    alias pmrm='pmnc -R'
    alias pmsr='pmnc -Ss'

} || { _check pacaur && {
    printf '\x1B[;1;36m~\x1B[m install \x1B[;1;4myay\x1B[m and uninstall \x1B[;1;4mpacaur\x1B[m\n'
    alias pm=pacaur
    alias pmnc='pm --noconfirm --noedit'

    alias pmupd='pmnc -Syu'

    alias pmin='pmnc -S'
    alias pmrm='pmnc -R'
    alias pmsr='pmnc -Ss'
}; }

# Apt-Get package manager
_check apt-get && {
    alias pm='sudo apt-get'
    alias pmnc='pm -y --no-install-recommends'
    alias pmupd='pmnc update && pmnc upgrade'

    alias pmin='pmnc install'
    alias pmrm='pmnc remove'
    alias pmsr='apt-cache search'
}

# Homebrew
_check brew && {
    alias pm='brew'
    alias pmnc='pm'
    alias pmupd='pmnc update && pmnc upgrade'

    alias pmin='pmnc install'
    alias pmrm='pmnc uninstall'
    alias pmsr='pmnc search'
}


# CUSTOM #
[[ -f "$HOME/.zshrc.custom" ]] && source "$HOME/.zshrc.custom"


# =========== #
# === END === #
export -U PATH="$PATH"
unalias _check

# END OF FILE - Anything below this comment block is an automatic insertion
# that should be refactored or removed - Automatic tools will alert me of
# anything below this so I can act upon it
# ----------------------------------- >8 ----------------------------------- #
