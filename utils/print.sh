function msg1 () {
    local fmt="$1"
    shift
    printf "\e[;1;96m==> \e[;1;97m$fmt\e[m\n" "$@"
}

function msg2 () {
    local fmt="$1"
    shift
    printf "  \e[;1;94m-> \e[;97m$fmt\e[m\n" "$@"
}

function die () {
    local code="$1"
    local fmt="$2"
    shift; shift
    printf "⚠️  \e[;1;91m$fmt\e[m\n" "$@"
    exit $code
}
