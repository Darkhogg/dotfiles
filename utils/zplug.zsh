source "$ZPLUG_HOME/init.zsh"
zplug 'zplug/zplug', from:github, hook-build:'zplug --self-manage'

# Oh-My-ZSH plugins
zplug 'plugins/archlinux', from:oh-my-zsh
zplug 'plugins/git', from:oh-my-zsh
zplug 'plugins/npm', from:oh-my-zsh
zplug 'plugins/sudo', from:oh-my-zsh
zplug 'plugins/systemd', from:oh-my-zsh
zplug 'plugins/virtualenv', from:oh-my-zsh
zplug 'plugins/virtualenvwrapper', from:oh-my-zsh

# Oh-My-ZSH defaults
zplug 'lib/completion', from:oh-my-zsh
zplug 'lib/directories', from:oh-my-zsh
zplug 'lib/history', from:oh-my-zsh
zplug 'lib/key-bindings', from:oh-my-zsh

export NVM_AUTO_USE=true
zplug 'lukechilds/zsh-nvm', from:github
zplug 'lukechilds/zsh-better-npm-completion', from:github, defer:3

# Spaceship Theme
zplug 'denysdovhan/spaceship-prompt', use:spaceship.zsh, from:github, as:theme

# ZSH-Users things
zplug "zsh-users/zsh-completions"
zplug "zsh-users/zsh-autosuggestions", defer:2
zplug 'hlissner/zsh-autopair', defer:2
zplug 'zsh-users/zsh-syntax-highlighting', defer:3
zplug "zsh-users/zsh-history-substring-search", defer:3

# Install plugins if necessary
zplug check || zplug install
